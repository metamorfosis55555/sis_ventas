
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>menu</title>

	<style>
		body{background-color: white;padding: 80px;font-family: Arial;}
		
		#menu{
			background-color: deepskyblue;

		}

		#menu ul{
			list-style: none;
			margin: 0;
			padding: 0;
		}

		#menu ul li{
			display: inline-block;
		}

		#menu ul li a{
			color: white;
			display: block;
			padding: 20px 20px;
			text-decoration: none;
		}

		#menu ul li a:hover{
			background-color: #42B881;
		}

		.item-r{
			float: right;
		}
	</style>
</head>
<body>
	<div id="menu">
		<ul>
			<li><a href="taclientes.php">clientes</a></li>
			<li><a href="tcp.php">catagoria de producto</a></li>
			<li><a href="taproductos.php">productos</a></li>
			<li><a href="tablaventaproductos.php">venta de productos </a></li>
			<li><a href="#">venta hechas </a></li>
			<li class="item-r"><a href="#">Contacto</a></li>
	