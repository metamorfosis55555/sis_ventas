
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Registro del estudiante</title>
        <link rel="stylesheet" type="text/css" href="../css/formu.css">
    </head>
    <body>
        <form action="regcliente.php" method="post" class="formulario" onsubmit="return validar();">
         
        <h1 class="formulario__titulo">registar cliente</h1>   
          
            <label for="" class="formulario__label">nombre </label>
            <input type="text" id="nombre" name="nombre" class="formulario__input" placeholder="Nombre del estudiante" required>
            
            <label for="" class="formulario__label">apellido </label>
            <input type="text" id="apellidos" name="apellidos" class="formulario__input"  placeholder="apellidos" required >
            
            <label for="" class="formulario__label">direccion </label>
            <input type="text" id="direccion" name="direccion" class="formulario__input"  placeholder="direccion" required>
            
            <label for="" class="formulario__label">correo </label>
            <input type="text" id="correo" name="correo"class="formulario__input"  placeholder="correo" required >
         
            <label for="" class="formulario__label">telelfono </label>
            <input type="text" id="telefono" name="telefono" class="formulario__input"  placeholder="telefono o celular" required>
             
            <input type="submit" value="Registrar" class="formulario__submit">
               
        </form>
    </body>
 
</html>