﻿# Host: localhost  (Version 5.5.5-10.1.34-MariaDB)
# Date: 2019-04-17 17:25:03
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Database "conva"
#

CREATE DATABASE IF NOT EXISTS `conva`;
USE `conva`;

#
# Structure for table "admi"
#

DROP TABLE IF EXISTS `admi`;
CREATE TABLE `admi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) DEFAULT NULL,
  `clave` bigint(20) unsigned zerofill NOT NULL DEFAULT '00000000000000000000',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "admi"
#

INSERT INTO `admi` VALUES (1,'edgar123',00000000000000002828),(2,'vicente',00000000000000003464);

#
# Structure for table "asignaturascruzadas"
#

DROP TABLE IF EXISTS `asignaturascruzadas`;
CREATE TABLE `asignaturascruzadas` (
  `codigoasignaturas` int(11) NOT NULL AUTO_INCREMENT,
  `materia` varchar(20) DEFAULT NULL,
  `nota` varchar(15) DEFAULT NULL,
  `codigoestudiante` int(4) DEFAULT NULL,
  PRIMARY KEY (`codigoasignaturas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "asignaturascruzadas"
#

INSERT INTO `asignaturascruzadas` VALUES (1,'calculo','58',2);

#
# Structure for table "carerra"
#

DROP TABLE IF EXISTS `carerra`;
CREATE TABLE `carerra` (
  `codigocarerra` int(11) NOT NULL AUTO_INCREMENT,
  `carerra` varchar(20) DEFAULT NULL,
  `gradoacademico` varchar(25) DEFAULT NULL,
  `univercidad` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`codigocarerra`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "carerra"
#

INSERT INTO `carerra` VALUES (1,'calculo','licianciatura','udabol');

#
# Structure for table "convalidacion"
#

DROP TABLE IF EXISTS `convalidacion`;
CREATE TABLE `convalidacion` (
  `codigoconvalidacion` int(11) NOT NULL AUTO_INCREMENT,
  `materiaconvalidada` varchar(20) DEFAULT NULL,
  `estadodeconvalidacion` varchar(20) DEFAULT NULL,
  `equivalencia` varchar(15) DEFAULT NULL,
  `notaconvalidada` int(3) DEFAULT NULL,
  `codigoestudiante` int(4) DEFAULT NULL,
  `codigomaterias` int(5) DEFAULT NULL,
  `codigomat` int(5) DEFAULT NULL,
  PRIMARY KEY (`codigoconvalidacion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "convalidacion"
#

INSERT INTO `convalidacion` VALUES (1,'r31r2r12r21r','r21rr12r2','r212r12',5888,1,1,0);

#
# Structure for table "estudiante"
#

DROP TABLE IF EXISTS `estudiante`;
CREATE TABLE `estudiante` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `carnet` int(8) DEFAULT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `apellidopaterno` varchar(20) DEFAULT NULL,
  `apellidomaterno` varchar(20) DEFAULT NULL,
  `fechanacimiento` varchar(25) DEFAULT NULL,
  `correo` varchar(25) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `telefono` int(10) DEFAULT NULL,
  `codigocarerra` int(5) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "estudiante"
#

INSERT INTO `estudiante` VALUES (1,3222,'tdhteth','hteyteyte','2252525','4352542','dgdg@gmil.com','tetet',54254,0),(2,14558,'edgar','vicente','mancilla','14/25/55','edgar@gmail.com','cochabamaba',74858,1);

#
# Structure for table "malla"
#

DROP TABLE IF EXISTS `malla`;
CREATE TABLE `malla` (
  `codigomateria` int(11) NOT NULL AUTO_INCREMENT,
  `materia` varchar(10) DEFAULT NULL,
  `carerra` varchar(11) DEFAULT NULL,
  `codigomat` int(11) DEFAULT NULL,
  PRIMARY KEY (`codigomateria`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "malla"
#

INSERT INTO `malla` VALUES (1,'2','633',0);

#
# Structure for table "mat"
#

DROP TABLE IF EXISTS `mat`;
CREATE TABLE `mat` (
  `codigomateria` int(11) NOT NULL AUTO_INCREMENT,
  `materia` varchar(45) DEFAULT NULL,
  `codigoconvalidacion` int(5) DEFAULT NULL,
  `codigomalla` int(5) DEFAULT NULL,
  PRIMARY KEY (`codigomateria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "mat"
#


#
# Database "tres"
#

CREATE DATABASE IF NOT EXISTS `tres`;
USE `tres`;

#
# Structure for table "tres"
#

DROP TABLE IF EXISTS `tres`;
CREATE TABLE `tres` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "tres"
#

INSERT INTO `tres` VALUES (1,'edgar','vicente');
